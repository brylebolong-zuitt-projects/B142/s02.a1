package com.bolong.b142.s02.s02app.services;

import com.bolong.b142.s02.s02app.models.Post;

public interface PostService {

    void createPost(Post newPost);
    void updatePost(Long id, Post updatedPost);
    void deletePost(Long id);
    Iterable<Post> getPosts();
}
