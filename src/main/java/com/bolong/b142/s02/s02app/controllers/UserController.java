package com.bolong.b142.s02.s02app.controllers;

import com.bolong.b142.s02.s02app.models.User;
import com.bolong.b142.s02.s02app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserService UserService;
    private String value;

    // Create a new user
    @RequestMapping(value="/User", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestBody User newUser){
        UserService.createUser(newUser);
        return new ResponseEntity<>("New user was created.", HttpStatus.CREATED);
    }

    // Retrieve all users
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<Object> getUser(){
        return new ResponseEntity<>(UserService.getUser(), HttpStatus.OK);
    }


    // Update an existing post
    @RequestMapping(value = "/User/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Object> updateUser(@PathVariable Long id, @RequestBody User updatedUser){
        UserService.updateUser(id, updatedUser);
        return new ResponseEntity<>("User was updated.", HttpStatus.OK);
    }


    // Delete a post
    @RequestMapping(value="/USer/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long id){
        UserService.deleteUser(id);
        HttpStatus body;
        return new ResponseEntity<>("User was deleted.", HttpStatus.OK);
    }
}
