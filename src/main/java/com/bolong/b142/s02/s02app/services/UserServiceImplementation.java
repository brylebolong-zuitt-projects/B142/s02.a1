package com.bolong.b142.s02.s02app.services;


import com.bolong.b142.s02.s02app.models.User;;
import com.bolong.b142.s02.s02app.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImplementation implements UserService{
    @Autowired
    private UserRepository UserRepository;



    public void createUser(User newUser) {
        UserRepository.save(newUser);
    }

    public void updateUser(Long id, User updatedUser){
      User existingUser = UserRepository.findById(id).get();
        existingUser.setUserName(updatedUser.getUserName());
        existingUser.setPassword(updatedUser.getPassword());
        UserRepository.save(existingUser);
    }


    public void deleteUser(Long id){
        UserRepository.deleteById(id);
    }

    public Iterable<User> getUser(){
        return UserRepository.findAll();
    }
}
